using Microsoft.VisualStudio.TestTools.UnitTesting;
using ShoppingBasket.Domain;
using System.Collections.Generic;

namespace ShoppingBaset
{
    [TestClass]
    public class ShoppingBasketFunctionTests
    {
        [TestMethod]
        public void Basket_has_1_bread_1_butter_and_1_milk_total_2_95()
        {
            List<BasketItem> basketItems = new List<BasketItem>
            {
                new BasketItem { Name = "Bread", Description= "Bread", Price = 1, Quantity=1},
                new BasketItem { Name = "Butter", Description= "Butter", Price = 0.8, Quantity=1},
                new BasketItem { Name = "Milk", Description= "Milk", Price = 1.15, Quantity=1},
            };

            double totalPriceShouldBe = 2.95;
            double totalDiscountShouldBe = 0;
            double grandTotalShouldBe = 2.95;

            IBasket shoppingBasket = new Basket(basketItems);
            shoppingBasket.CalculatePrice();

            Assert.AreEqual(totalPriceShouldBe, shoppingBasket.TotalPrice);
            Assert.AreEqual(totalDiscountShouldBe, shoppingBasket.TotalDiscountApplied);
            Assert.AreEqual(grandTotalShouldBe, shoppingBasket.GrandTotal);
            Assert.AreEqual(shoppingBasket.TotalPrice, shoppingBasket.GrandTotal);
        }

        [TestMethod]
        public void Basket_has_2_butters_and_2_breads_total_3_1()
        {
            List<BasketItem> basketItems = new List<BasketItem>
            {
                new BasketItem { Name = "Bread", Description= "Bread", Price = 1, Quantity=2 },
                new BasketItem { Name = "Butter", Description= "Butter", Price = 0.8, Quantity=2 },
            };

            double totalPriceShouldBe = 3.6;
            double totalDiscountShouldBe = 0.5;
            double grandTotalShouldBe = 3.1;

            IBasket shoppingBasket = new Basket(basketItems);
            shoppingBasket.CalculatePrice();

            Assert.AreEqual(totalPriceShouldBe, shoppingBasket.TotalPrice);
            Assert.AreNotEqual(0, shoppingBasket.TotalPrice);
            Assert.AreEqual(totalDiscountShouldBe, shoppingBasket.TotalDiscountApplied);
            Assert.AreNotEqual(0, shoppingBasket.TotalDiscountApplied);
            Assert.AreEqual(grandTotalShouldBe, shoppingBasket.GrandTotal);
            Assert.AreNotEqual(shoppingBasket.TotalPrice, shoppingBasket.GrandTotal);
        }

        [TestMethod]
        public void Basket_has_4_milks_total_3_45()
        {
            List<BasketItem> basketItems = new List<BasketItem>
            {
                new BasketItem { Name = "Milk", Description= "Milk", Price = 1.15, Quantity=4 },
            };

            double totalPriceShouldBe = 4.6;
            double totalDiscountShouldBe = 1.15;
            double grandTotalShouldBe = 3.45;

            IBasket shoppingBasket = new Basket(basketItems);
            shoppingBasket.CalculatePrice();

            Assert.AreEqual(totalPriceShouldBe, shoppingBasket.TotalPrice);
            Assert.AreNotEqual(0, shoppingBasket.TotalPrice);
            Assert.AreEqual(totalDiscountShouldBe, shoppingBasket.TotalDiscountApplied);
            Assert.AreNotEqual(0, shoppingBasket.TotalDiscountApplied);
            Assert.AreEqual(grandTotalShouldBe, shoppingBasket.GrandTotal);
            Assert.AreNotEqual(shoppingBasket.TotalPrice, shoppingBasket.GrandTotal);
        }

        [TestMethod]
        public void Basket_has_2_butter_1_bread_and_8_milk_total__9()
        {
            List<BasketItem> basketItems = new List<BasketItem>
            {
                new BasketItem {Name = "Bread", Description= "Bread", Price = 1, Quantity=1},
                new BasketItem {Name = "Butter", Description= "Butter", Price = 0.8, Quantity=2},
                new BasketItem {Name = "Milk", Description= "Milk", Price = 1.15, Quantity=8},
            };

            double totalPriceShouldBe = 11.8;
            double totalDiscountShouldBe = 2.8;
            double grandTotalShouldBe = 9;

            IBasket shoppingBasket = new Basket(basketItems);
            shoppingBasket.CalculatePrice();

            Assert.AreEqual(totalPriceShouldBe, shoppingBasket.TotalPrice);
            Assert.AreNotEqual(0, shoppingBasket.TotalPrice);
            Assert.AreEqual(totalDiscountShouldBe, shoppingBasket.TotalDiscountApplied);
            Assert.AreNotEqual(0, shoppingBasket.TotalDiscountApplied);
            Assert.AreEqual(grandTotalShouldBe, shoppingBasket.GrandTotal);
            Assert.AreNotEqual(shoppingBasket.TotalPrice, shoppingBasket.GrandTotal);
        }
    }
}
