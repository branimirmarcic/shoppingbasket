﻿using System.Collections.Generic;

namespace ShoppingBasket.Domain
{
    public interface IBasket
    {
        List<BasketItem> BasketItems { get; set; }
        double TotalPrice { get; set; }
        double TotalDiscountApplied { get; set; }
        double GrandTotal { get; set; }
        void CalculatePrice();
    }
}