﻿using System;
using System.Collections.Generic;
using System.IO;

namespace ShoppingBasket.Domain
{
    public class Basket : IBasket
    {
        public List<BasketItem> BasketItems { get; set; }
        public double TotalPrice { get; set; }
        public double TotalDiscountApplied { get; set; }
        public double GrandTotal { get; set; }

        public Basket(List<BasketItem> items)
        {
            this.BasketItems = items;
            this.TotalDiscountApplied = 0;
            this.GrandTotal = 0;
            this.TotalPrice = 0;
        }

        public void CalculatePrice()
        {
            foreach (var item in this.BasketItems)
            {
                var itemTotalPrice = item.Price * item.Quantity;
                TotalPrice = Math.Round(TotalPrice + itemTotalPrice, 2);
            }
            TotalDiscountApplied = CalculatePriceDiscount();
            GrandTotal = Math.Round(TotalPrice - TotalDiscountApplied, 2);

            LogItemsFromBasket(this.BasketItems, TotalPrice, TotalDiscountApplied, GrandTotal);
        }

        double CalculatePriceDiscount()
        {
            var numberOfButter = this.BasketItems.Find(x => x.Name.Equals("Butter"))?.Quantity ?? 0;
            var numberOfBread = this.BasketItems.Find(x => x.Name.Equals("Bread"))?.Quantity ?? 0;
            var numberOfMilk = this.BasketItems.Find(x => x.Name.Equals("Milk"))?.Quantity ?? 0;

            double totalBreadForDiscount = 0;
            for (int i = 0; i < numberOfButter; i++)
                if (i % 2 != 0)
                    if (i < numberOfButter)
                        totalBreadForDiscount += (this.BasketItems.Find(x => x.Name.Equals("Bread"))?.Price ?? 0) * 0.5;
                    else
                        break;

            double totalMilkForDiscount = 0;
            for (int i = 1; i <= numberOfMilk; i++)
                if (i % 3 == 0)
                    if (i - 1 < numberOfMilk)
                        totalMilkForDiscount += this.BasketItems.Find(x => x.Name.Equals("Milk"))?.Price ?? 0;
                    else
                        break;

            return totalBreadForDiscount + totalMilkForDiscount;
        }

        void LogItemsFromBasket(List<BasketItem> basketItems, double totalPrice, double totalDiscountApplied, double grandTotal)
        {
            string print = string.Empty;
            print += $"DateTime: {DateTime.Now}";
            print += $"{Environment.NewLine}";

            foreach (var item in basketItems)
                print += $"Name: {item.Name}, Quantity: {item.Quantity}, Price: {item.Price} {Environment.NewLine}";

            print += $"{Environment.NewLine}";
            print += $"Total price: Price: {totalPrice} {Environment.NewLine}";
            print += $"Total Discount Applied: {totalDiscountApplied} {Environment.NewLine}";
            print += $"Grand total: {grandTotal} {Environment.NewLine}";
            print += $"-----------------------------------------------------------------------------------------------";
            print += $"{Environment.NewLine}";

            File.AppendAllText(AppDomain.CurrentDomain.BaseDirectory + "\\log.txt", print);
        }
    }
}
