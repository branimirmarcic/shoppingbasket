﻿using ShoppingBasket.Domain;
using System;
using System.Collections.Generic;

namespace ShoppingBasketApp
{
    class Program
    {
        static void Main(string[] args)
        {
            List<BasketItem> basketItems = new List<BasketItem>
            {
                new BasketItem { Name = "Bread", Description= "Bread", Price = 1, Quantity=10},
                new BasketItem { Name = "Butter", Description= "Butter", Price = 0.8, Quantity=20},
                new BasketItem { Name = "Milk", Description= "Milk", Price = 1.15, Quantity=80},
            };

            IBasket cart = new Basket(basketItems);
            cart.CalculatePrice();
            foreach (var item in basketItems)
                Console.WriteLine($"Name: {item.Name}, Quantity: {item.Quantity}, Price: {item.Price}");

            Console.WriteLine($"{Environment.NewLine}");
            Console.WriteLine($"Total price: {cart.TotalPrice}");
            Console.WriteLine($"Total Discount Applied: {cart.TotalDiscountApplied}");
            Console.WriteLine($"Grand total: {cart.GrandTotal}");
            Console.ReadLine();
        }
    }
}
